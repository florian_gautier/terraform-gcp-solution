terraform {
  backend "http" {
  }
}
provider "google" {

  credentials = file("gcp_credentials.json")

  project = "dog-1-349208"
  region  = "us-central1"
  zone    = "us-central1-c"
}

resource "google_compute_instance" "tp-gitlab-1" {

  name         = "tp-gitlab-1"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    network = "default"

    access_config {

    }
  }


}

